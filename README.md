# Discord RPC Emulator

[![build status](https://gitlab.com/teppyboy/discordrpcemulator/badges/master/build.svg)](https://gitlab.com/teppyboy/discordrpcemulator/commits/master)
[![coverage report](https://gitlab.com/teppyboy/discordrpcemulator/badges/master/coverage.svg?job=coverage)](https://gitlab.com/teppyboy/discordrpcemulator/commits/master)
[![Code Climate](https://codeclimate.com/github/gitlabhq/gitlabhq.svg)](https://codeclimate.com/github/gitlabhq/gitlabhq)
[![Core Infrastructure Initiative Best Practices](https://bestpractices.coreinfrastructure.org/projects/42/badge)](https://bestpractices.coreinfrastructure.org/projects/42)

## About

This is an app that help you emulate your Discord Rich Presence to anything you want using [this](https://github.com/Lachee/discord-rpc-csharp)

## Editions

There are one editions of Discord RPC Emulator:

- Discord RPC Emulator .NET Framework (Windows only)
- Discord RPC Emulator .NET Core (Windows, macOS x64, Linux x64)

## Requirements

For Discord RPC Emulator .NET Framework:
- .NET Framework 4.8 or higher installed.

For Discord RPC Emulator .NET Core:
-  [Microsoft Visual C++ Redistributable for Visual Studio 2015, 2017 and 2019 x86 package](https://aka.ms/vs/16/release/vc_redist.x86.exe) (Windows only)
-  .NET Core SDK 3.1 for Windows build (macOS might need if the compiled build isn't working since I don't have a Mac to test)
-  NET Core Notice: File name: AquaDRPCE.(executable format, for linux its empty and on Linux do chmod +x AquaDRPCE before use)

## Installation

Download latest build on [releases](https://gitlab.com/teppyboy/discordrpcemulator/-/releases/)

## Install a development environment

For Visual Studio 2019:
- Have developing app using .NET Framework
- .NET Framework 4.8 SDK installed
- .NET Core 3.1 SDK installed

For Visual Studio Code:
- C# extension by Microsoft (aka OmniSharp)
- .NET Core 3.1 SDK installed

For Hardcore Developers:
- Edit files using notepad/gedit or any text editor you like (Hex editor is recommended xD)
- Compile using msbuild MSBuild.exe DIscordRPCEmulator.sln -property:Configuration=Debug (or MSBuild.exe DIscordRPCEmulator.sln -property:Configuration=Release for release)

## Building

Discord RPC Emulator NET Framework:
- Build the project like normal way in VS 2019/MonoDevelop/VS Code

Discord RPC Emulator .NET Core:
- Windows: run dotnet-build.bat
- Linux + macOS: run dotnet-build.sh (for macOS do sh dotnet-build.sh)
This script will build Debug + Release for Windows,macOS,Linux
Make sure to have .NET Core 3.1 SDK before build (this is the problem why CI can't build)

## Third-party applications

[Discord RPC by Lachee](https://github.com/Lachee/discord-rpc-csharp)

## Documentation

CS

## Getting help

For now create an issue if you have any problem

### This README is based on https://gitlab.com/gitlab-org/gitlab-foss/-/raw/idea-to-production-demo/README.md
